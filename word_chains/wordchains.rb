require "set"
class WordChainer

  attr_reader :dictionary
  
  def initialize(dict_file="dictionary.txt")
    @dictionary = get_words(dict_file).to_set
  end
  
  def get_words(file)
    File.readlines(file).map { |word| word.chomp }
  end
  
  def adjacent_words(base_word)
    words = @dictionary.select do |candidate|
      right_length?(candidate, base_word) && one_off?(candidate, base_word)
    end
    words
  end
  
  
  def right_length?(candidate, base)
    base.length == candidate.length
  end
  
  def one_off?(candidate, base)
    diffs = 0
    candidate_letters = candidate.split("")
    base_letters = base.split("")
    
    base_letters.each_with_index do |char, i|
      diffs += 1 if char != candidate_letters[i]
    end
    diffs == 1
  end
  
  def run(source, target)
    @current_words = [source]
    @all_current_words = { source => nil }
    
    until @current_words.empty?
      explore_current_words
    end
    
    build_path(target)
  end
  
  def explore_current_words
    @current_words.each do |current_word|
      new_current_words = []
      adjacent_words(current_word).each do |adj_word|
        next if @all_current_words.include? adj_word
        new_current_words << adj_word
        @all_current_words[adj_word] = current_word 
      end
    
      #new_current_words.each { |k| puts "Key: #{k}; Value: #{@all_current_words[k]}"}
      @current_words = new_current_words
    end
  end
  
  def build_path(target)
    parent = @all_current_words[target]
    return [target] if parent.nil?
    
    path = [target]
    path + (build_path(parent))
  end
end

def make
  WordChainer.new("dictionary.txt")
end