class WordChainer
  
  def initialize(dict_file)
    @dictionary = get_words(dict_file).to_set
  end
  
  def get_words(file)
    lines = File.readlines("dictionary.txt").map { |word| line.chomp }
  end
  
  def adjacent_words(base_word)
    @dictionary.select do |candidate|
      right_length?(candidate, base_word) && one_off?(candidate, base_word)
    end
  end
  
  
  def right_length(candidate, base)
    base.length == candidate.length
  end
  
  def one_off?(candidate, base)
    diffs = 0
    candidate.each_char do |cand_char|
      base.each_char do |base_char|
        diffs += 1 if cand_char != base_char
      end
    end
    diffs == 1
  end
  
end