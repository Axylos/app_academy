class MyHashSet
  attr_accessor :store
  
  def initialize()
    @store = {}
  end
  
  def insert(el)
    @store[el] = true
  end
  
  def include?(el)
    @store.has_key?(el)
  end
  
  def delete(el)
    @store.delete(el) ? true : false
  end
  
  def to_a
    @store.keys
  end
  
  def intersect(set2)
    self.to_a & set2.to_a
  end
  
  def union(set2)
    (self.to_a + set2.to_a).uniq.sort
  end
  
  def minus(set2)
    newset = self
    set2.to_a.each do |key|
      newset.delete key
    end
    newset
  end
end