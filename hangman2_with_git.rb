class Hangman

  def initialize(guessing_player, checking_player)
    @guessing_player = guessing_player
    @checking_player = checking_player
    @turns = 10
    @guessed_letters = []
    @won = false
  end
  
  def get_secret_word
    
  end
  
  def get_guess
    puts "You have #{@turns} turns left."
    puts "You have guessed: #{@guessed_letters}."
    
    guess_letter = @guessing_player.guess(@letter_blanks)
  end
  
  def handle_guess_response(guess)
    matches = @checking_player.check_guess(guess)
    @turns -= 1 if matches.count < 1
    matches.each { |match| @letter_blanks[match] = guess }
    @guessed_letters << guess
    @letter_blanks.none? { |char| char.nil? }
  end
  
  def print_secret_word
    puts "The word was #{@checking_player.get_secret}!"
  end
  
  def run
    secret_word_length = get_and_swap_secret_length
    @letter_blanks = Array.new(secret_word_length)
    
    while @turns > 0 && !@won
      
      guess_letter = get_guess
      
      won = handle_guess_response(guess_letter)
      
      if won
        puts "The guessing player won!"
        print_secret_word
        
        return
      end
     
    end
    puts "The guessing player lost!"
    print_secret_word
  end
  
  def get_and_swap_secret_length
    length = @checking_player.give_secret_length
    @guessing_player.receive_secret_length(length)
    length
  end
end

class HumanPlayer
  
  def give_secret_length
    word_length = 0
    until word_length.is_a?(Integer) && word_length > 1  
      puts "Enter the length of the secret word"
      word_length = gets.chomp.to_i
    end
    word_length
  end
  
  def receive_secret_length(length)
    puts "the length of the secret word is: #{length}"
  end
  
  def prompt(blanks)
    letters = blanks.map { |el| el.nil? ? "_" : el}.join(" ")
    puts "The blanks are: #{letters}"
    puts "Guess a letter: "
  end
  
  def guess(letter_blanks)
    prompt(letter_blanks)
    gets[0].downcase
  end
  
  def check_guess(guess)
    puts "Enter matches in the secret word for #{guess}"
    hits = gets.chomp
    p matches = hits.scan(/\d+/).map { |el| el.to_i }
  end
  
  def get_secret
    puts "What was your word?"
    gets.chomp
  end
  
end


class ComputerPlayer
  
  def initialize(dict_file="dictionary.txt")
    @words = File.readlines("dictionary.txt").map { |line| line.chomp }
    @guessed_letters = []
  end
  
  def get_secret
    @picked_word
  end
  
  def pick_word(dict_file="dictionary.txt")
    @picked_word = @words.sample
  end
  
  def guess(letter_blanks)
    guess = compute_smart_guess(letter_blanks)
    @guessed_letters << guess
    guess
  end
  
  def get_max(words)
    chars = words.join.split("")
    letters = ("a".."z").reject { |char| @guessed_letters.include? char }
    letters.to_a.max_by { |char| chars.count char }
  end
  
  
  def get_right_length(length)
    @words.select { |word| word.length == length }
  end
  
  def filter_wrong_position(words, blanks)
    filtered_words = words.select do |word|
      
      diffs = 0
      blanks.each_with_index do |char, i|
        
        diffs += 1 if char != word[i]
      end
      diffs.zero?
        
    end
    filtered_words
  end
  
  def match_wrong_place_filter(words, blanks)
    words.select do |word|
      diffs = 0
      
      words.each_with_index do |char, i|
        if blanks.include?(char) && blanks[i] != char
          diffs += 1
        end
      end
      diffs.zero? 
        
    end
  end
  
  def filter_failed_guesses(words, blanks)
    chars = @guessed_letters.select { |letter| !(blanks.include?(letter)) }
    words.select { |word| word.split("") != word.split("") - chars }
  end
  
  def compute_smart_guess(blanks)
    right_length_words = get_right_length(blanks.count)
    right_guessed_words = filter_wrong_position(right_length_words, blanks)
    includes_failed_guess = filter_failed_guesses(right_length_words, blanks)
    final_words = match_wrong_place_filter(includes_failed_guess, blanks)
    p final_words
    get_max(final_words)
  end
  
  def check_guess(guess)
    (0...@picked_word.length).to_a.select { |i| @picked_word[i] == guess }
  end
  
  def give_secret_length
    pick_word.length
  end
  
  def receive_secret_length(length)
    @secret_word_length = length
  end
end