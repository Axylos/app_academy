def caesar(str, cipher)
  chars = str.split("")
  chars.map! do |c| 
    pos = c.ord + cipher
    (pos > "z".ord ? pos -= 26 : pos).chr
  end
end