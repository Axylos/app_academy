def median(arr)
  if arr.length.odd?
    arr[arr.length / 2]
  else
    (arr[((arr.length / 2) - 1)] + arr[(arr.length / 2)]) / 2.to_f 
  end
end


