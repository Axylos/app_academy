require "pry"

class Board
  
  attr_accessor :winner
  
  def initialize
    @board = Array.new(3) { Array.new(3) }
  end
  
  def valid_moves
    valid_moves = []
    @board.each_with_index do |row, i|
      row.each_index do |pos|
        valid_moves << [i, pos] if self.empty?([i, pos])
      end
    end
    valid_moves
  end
  
  def smart_moves(marker)
    winning_moves = []
    smart_rows = []
    smart_cols = []
    @board.each_index do |i| 
      if (@board[i].count(marker) == 2) && (@board[i].include?(nil))
        smart_rows << i
      end
    end
    @board.each_index do |i|
      if (@board[i].count(marker) == 2) && (@board[i].include?(nil))
        smart_cols << i
      end
    end
    
    smart_rows.each do |row_index|
      pos = @board[row_index].index(nil)
      winning_moves << [row_index, pos]
    end
    
    smart_cols.each do |col_index|
      pos = @board[col_index].index(nil)
      winning_moves << [pos, col_index]
    end
    
    diags = [[[0, 0], [1, 1], [2, 2]], [[0, 2], [1, 1], [2, 0]]]
    
    diags.keep_if do |diag|
      array = diag.map { |el| @board[el.first][el.last] }
      array.count(marker) == 2 && array.include?(nil)
    end
    
    diags.each do |diag|
      diag.each do |diag1|
        winning_moves << [diag1.first, diag1.last] if (@board[diag1.first][diag1.last]).nil?
      end
    end  
    winning_moves  
  end
  
  def empty?(pos)
    row, col = pos[0], pos[1]
    @board[row][col].nil?
  end
  
  def won?(board=@board)
    row_win?(board) || row_win?(board.transpose) || diagonal_win?(board) || tie?
  end
  
  def tie?
    if valid_moves.count.zero?
      @winner = :tie
      return true
    else
      return false
    end
  end
  
  def diagonal_win?(board)
    first = [board[0][0], board[1][1], board[2][2]]
    second = [board[0][2], board[1][1], board[2][0]]
    if first.all? { |el| el == :X } || second.all? { |el| el == :X }
      @winner = :X
      return true
    elsif second.all? { |el| el == :O } || first.all? { |el| el == :O }
      @winner = :O
      return true
    else
      false
    end
  end
  
  def row_win?(board_state)
    if board_state.any? do |row| 
        row.all? { |el| el == :X } 
      end
      @winner = :X
      return true
    elsif board_state.any? do |row| 
        row.all? { |el| el == :O }
      end
      @winner = :O
      return true
    end
    return false
  end
  
  def place_mark(pos, mark)
    row, col = pos[0], pos[1]
    @board[row][col] = mark
    render
  end
  
  def render
    puts "#{@board[0][0] || "_"}|#{@board[0][1] || "_"}|#{@board[0][2] || "_"}"
    puts "#{@board[1][0] || "_"}|#{@board[1][1] || "_"}|#{@board[1][2] || "_"}"
    puts "#{@board[2][0] || "_"}|#{@board[2][1] || "_"}|#{@board[2][2] || "_"}"
  end
end