def rps(user_move)
  computer_move = ['rock', 'paper', 'scissors'].sample
  lose = "You lose, #{computer_move}"
  win = "You win, #{computer_move}"
  
  case 
  when user_move == computer_move
    puts "You both picked #{user_move}"
    
  when user_move == 'rock' 
    if computer_move == 'paper'
      puts lose
    else
      puts win
    end
    
  when user_move == 'paper'
    if computer_move == 'scissors'
      puts lose
    else
      puts win
    end
    
  when user_mover == 'scissors'
    if computer_move == 'rock'
      puts lose
    else
      puts win
    end
  end
  
end