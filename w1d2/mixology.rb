def remix(drink_array)
  soda = []
  
  mixers = drink_array.map do |drink|
    drink[1]
  end.shuffle
  remix_array = drink_array.map do |drink|
    mixer = mixers.pop
    [drink[0], mixer]
  end
  
  if remix_array.any? {|drink| drink_array.include? drink}
    remix(remix_array)
  end
  
  remix_array
end


