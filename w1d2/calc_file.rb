require "./input_output.rb"

expressions = File.read ARGV[0]
expr_arry = (expressions.delete "\n").split(" ")

rpn = RPN.new
puts "The value is: #{rpn.parse_arry expr_arry}"