require "./players.rb"
require "./board.rb"

class Game
    
  attr_accessor :board
  def initialize(player1, player2)
    @player1 = player1
    @player2 = player2
    @board = Board.new
  end
  
  def game_move(player, marker)
    pick = player.move(@board.valid_moves, @board.smart_moves(marker))
    @board.place_mark(pick, marker)
  end
    
  def valid(pick)
    @board.valid_moves.include? pick
  end
    
  def play
    loop do
      game_move(@player1, :X)
      break if @board.won?
      game_move(@player2, :O)
      break if @board.won?
    end
    if @board.winner == :tie
      puts "It's a tie!"
    else
      puts "#{@board.winner} won!"
    end
  end
end