class ComputerPlayer
  
  def move(valid_moves, winning_moves)
    if !winning_moves.empty?
      puts "I'm gonna win with #{winning_moves.first}"
      winning_moves.first
    else
      valid_moves.sample
    end
  end
end

class HumanPlayer
  def move(valid_moves, winning_moves)
    puts "Enter a move: "
    pick = gets.chomp.split(" ").map { |char| char.to_i }
    if valid_moves.include?(pick)
      return pick
    else
      puts "Invalid move."
      move(valid_moves, winning_moves)
    end
  end
end

