def super_print(str, options = {})
  default = {
    :times => 3,
    :upcase => true,
    :reverse => true
  }
  
  super_string = ""
  
  default = default.merge options
  str.upcase! if default[:upcase] == true
  str.reverse! if default[:reverse] == true
  default[:times].times do 
    super_string << "#{str} " 
  end
  
  super_string.strip
end
