def num_loop(floor = 250, div = 7)
  i = 0
  until i > 250 && (i%7).zero?
    i += 1 
  end
  i
end

def factors(num)
  factors = (1..num/2).select {|fact| (num%fact).zero?}
end

def bubble_sort(array)
  changes = 1
  
  until changes == 0
    changes = 0
    (array.count - 1).times do |i|
      if array[i] > array[i+1]
        array[i], array[i+1] = array[i+1], array[i]
        changes += 1
      end
    end
  end
  
  array 
end

def substrings(str)
  subarray = []
  
  letters = str.split("")
  letters.each_index do |start|
    start.upto(letters.count - 1) do |final|    
      subarray << letters[start..final].join
    end
  end
  
  subarray.flatten
end

def subwords(str)
  real_words = substrings(str)

  dictionary = File.readlines "./dictionary.txt"
  dictionary.map! {|line| line.chomp}
  
  real_words.keep_if do |substring| 
    dictionary.include? substring
  end
  
  real_words
end









