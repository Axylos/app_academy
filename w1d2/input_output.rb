class GuessingGame
  
  def initialize
    @pick = rand(100)
  end
  
  def prompt
    puts "Enter a guess: "
    gets.chomp.to_i
  end

  def play
    loop do
      case self.prompt <=> @pick
      when 1
        puts "Too High!"
      when 0
        puts "You Win!"
        break
      when -1
        puts "Too Low!"
      end
    end
  end
  
end


class RPN
  
  def initialize
    @stack = []
  end
  
  def calc
    user_input = nil
    until user_input == "value" do 
      puts "Enter a number or operator"
      user_input = gets.chomp
      break if user_input == 'quit'
      eval user_input 
    end
  end
  
  def parse_arry(arry)
    arry.each { |char| eval char }
    @stack.last
  end
  
  
  private
  
  def eval(input)
    operators = %w[/ * + -]
    if input == 'value'
      puts "value is #{@stack.last}"
    elsif !operators.include?(input)
      push input.to_i
    else
      val(input.to_sym)
    end
  end
  
  def push(num)
    @stack << num
  end
  
  def val(operator)
    nums = @stack.pop 2
    @stack << nums.inject(&operator)
  end
  
end



























  