def prompt
  puts "Give me the name of a file"
  gets.chomp
end

def writefile(name, shuffled_lines)
  new_name = name_file(name)
  new_file = File.open(new_name, 'w') do |f|
    shuffled_lines.each do |line| 
      next if line.empty?
      f.puts line
    end
  end
end

def line_shuffle(file)
  lines = File.readlines(file)
  lines.shuffle
end

def name_file(str)
  str = str.gsub!(".txt", "")
  "#{str}-shuffled.txt"
end

old_file = prompt

shuffled_lines = line_shuffle(old_file)
writefile(old_file, shuffled_lines)

