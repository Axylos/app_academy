class Student
  
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name  
    @courses = []
  end
  
  def name
    "#{@first_name} #{@last_name}"
  end
  
  def courses
    @courses
  end
  
  def enroll(course)
    raise "Schedule Conflict!" if has_conflict?(course)
    @courses << course unless @courses.include? course
  end
  
  def has_conflict?(course)
    @courses.any? { |enrolled| enrolled.conflicts_with?(course) }
  end
  
  def course_load
    student_load = {}
    @courses.each do |course|
      if student_load.keys.include? course.dept
        student_load[course.dept] += course.credits 
      else
        student_load[course.dept] = course.credits
      end
    end
    student_load
  end
end

class Course
  
  attr_accessor :name, :dept, :credits, :time
  
  def initialize(name, dept, credits, days, time)
    @name = name
    @dept = dept
    @credits = credits
    @days = days
    @time = time
  end
  
  def add_student(student)
    @students << student
  end
  
  def students
    puts @students
  end
  
  def conflicts_with?(other_course)
  @days.any? do |day| 
      other_course.days.include? day 
    end && @time == other_course.time
  end
end








