def mult_two(arr)
  arr.map {|x| x * 2}
end

class Array
  def my_each
    x = 0
    while x < self.length
      yield self[x]
      x += 1
    end
    self
  end 
end

