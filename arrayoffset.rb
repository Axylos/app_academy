def correct_hash(hash)
  h = {}
  hash.each_pair{|k,v| h[k.next] = v}
  h
end

wrong_hash = { :a => "banana", :b => "cabbage", :c => "dental_floss", :d => "eel_sushi" }
p correct_hash(wrong_hash)