require "pry"
f = File.new("./maze.txt")

@lines = f.readlines
@lines.each {|line| puts line}

srow = @lines.index do |line|
   line.include?("S")
 end

scol = @lines[srow].index("S") 

def get_moves(position)
  moves = {:up => position[0]+1, :down => position[0]-1, :right
  }
end

def is_valid? (move)
  #puts move.inspect
  @lines[move[0]][move[1]] != "*" && @lines[move[0]][move[1]] != "X"
end

def move(direction, current_pos)
  puts current_pos.inspect
  move = current_pos
  case direction
  when :up 
    move[0] -= 1
  when :down
    move[0] += 1 
  when :left
    move[1] -= 1
  when :right
    move[1] += 1
  end
  
  if is_valid? move
    p "hey" 
    current_pos = move
    @lines[current_pos[0]][current_pos[1]] = "X"
  else
    return false
  end
  current_pos
end

current_pos = [srow, scol]
current_char = @lines[current_pos[0]][current_pos[1]]

while current_char != "E"
  moves = get_moves(current_pos)
  
  
  current_char = @lines[current_pos[0]][current_pos[1]]
  @lines.each {|line| puts line}
end