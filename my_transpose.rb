def my_transpose(mtx)
  answer = []
  mtx.count.times do
    answer << []
  end
  
  mtx.each_with_index do |arr, row|
    arr.each_with_index do |el, col|
      answer[col][row] = el
    end
  end
  answer
end

puts my_transpose([
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8]
    ]).inspect