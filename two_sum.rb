def two_sum(array)
  first = second = 0
  answer = []
  while first < array.length
    second = first + 1
    while second < array.length
      answer << [first, second] if array[first] + array[second] == 0
      second += 1
    end
    first += 1
  end
  answer
end

p two_sum([-1, 0 , 2, -2 , 1])