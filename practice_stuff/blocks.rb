def ranger(start, last)
  
  return [] if last < start
  
  ranger(start, last - 1) + [last]
end

def recur1(base, num)
  return 1 if num == 0
  base * recur1(base, num - 1)
  
end

def recur2(base, num)
  
  return 1 if num == 0
  return base if num == 1
  
  if num.even?
    recur2(base, num/ 2) ** 2
  else
    
    base * (recur2(base, (num - 1) / 2) ** 2)
  end
end

class Array
  def deep_dup
    return self unless self.is_a?(Array)
  
    self.map do |el|
     el.is_a?(Array) ? el.deep_dup : el
    end
  end
end

def fib(n)
  
  return [1] if n == 1
  
  fibs = fib(n - 1)
  fibs << (fibs.last 2).inject(:+)
end

def binary_search(coll, target)
  return nil if coll.empty?
    
  case coll[coll.count / 2] <=> target
  when 1
    binary_search(coll.take(mid), target) 
  when 0
    return mid
  when -1
    if coll.count.odd?
      binary_search(coll.drop(mid + 1), target) + mid + 1
    else
      binary_search(coll.drop(mid), target) + mid
    end
  end
end


class Array
  
  def merge_sort
    return [] if self.count < 1
    return self  if self.count == 1

    mid = self.count / 2
    if self.count.odd?
      first = self.take mid
      second = self.drop(mid)
    else
      first = self.take mid
      second = self.drop mid
    end


   [] + merge(first.merge_sort, second.merge_sort)
  end
    
  def merge(first, second)
    new_array = []
   
    until first.empty? || second.empty?
      if first[0] > second[0]
        new_array << second.shift
      else
        new_array << first.shift
      end
    end
    
    
   new_array + first + second
  end
end





