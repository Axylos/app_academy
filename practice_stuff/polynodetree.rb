class PolyTreeNode
  
  attr_reader :parent, :children, :value
  def initialize(value)
    @value = value
    @parent = nil
    @children = []
  end
  
  def parent=(parent)
    unless @parent.nil?
      @parent.children.delete self
    end
    @parent = parent
    @parent.children << self unless parent.nil?
  end
  
  def add_child(child_node)
    child_node.parent = self
  end
  
  def remove_child(child)
    self.delete child
  end
  
  def dfs(value)
    return self if self.value == value
    
    self.children.each do |child|
      val = child.dfs(value)
      return val if val
    end
    nil
  end
  
  def bfs(value)
    
    node_queue = [self]
    
    until node_queue.empty?
      
      candidate_node = node_queue.shift
      return candidate_node if candidate_node.value == value
      node_queue += candidate_node.children
    end
    nil
  end
end



p = PolyTreeNode.new(1)
x = PolyTreeNode.new(2)
p.add_child(x)
y = PolyTreeNode.new(3)
p.add_child(y)

z = PolyTreeNode.new(4)
x.add_child(z)

a = PolyTreeNode.new(5)
p.add_child(a)
