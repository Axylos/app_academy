@num_discs = 2
@win = (1..@num_discs).to_a.reverse

@towers = [@win, [], []]

game_over = false

def is_valid?(from, place)
  return false if !from.between?(0,2) || !place.between?(0,2)
  
  from = @towers[from]
  place = @towers[place]

  return false if from.empty?
  return true if place.empty?
  from.last < place.last
end

def move_disc(from, place)
  if is_valid?(from, place)
    disc = @towers[from].pop
    @towers[place] << disc
  end
end

def success?
  @towers[1] == @win || @towers[2] == @win
end

def display_towers
  puts "\nTower 1: #{@towers[0].inspect}"
  puts "Tower 2: #{@towers[1].inspect}"
  puts "Tower 3: #{@towers[2].inspect}\n"
end

while !game_over
  display_towers
  
  puts "Enter a tower to take from:"
  from = gets.chomp.to_i - 1

  puts "Enter a tower to place on:"
  place = gets.chomp.to_i - 1
  
  is_valid?(from, place) ? move_disc(from, place) : (puts "Invalid move. Try again.\n\n")
  
  game_over = true if success?
end

puts "You Won!"
display_towers