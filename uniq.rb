def my_uniq(array)
  answer = []
  array.each do |num|
    answer << num unless answer.include? (num)
  end
  answer
end

p my_uniq([1, 2, 1, 3, 3])