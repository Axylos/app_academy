def num_to_s(num, base)
  nums = {0 => "0",
          1 => "1", 
          2 => "2",
          3 => "3",
          4 => "4",
          5 => "5",
          6 => "6",
          7 => "7",
          8 => "8",
          9 => "9",
          10 => "A",
          11 => "B",
          12 => "C",
          13 => "D",
          14 => "E",
          15 => "F"}
  
  pow = 0
  answer = ""
  while num / (base ** pow) != 0
    digit = (num / (base ** pow)) % base
    answer << nums[digit]
    pow += 1
  end
  answer.reverse
end

